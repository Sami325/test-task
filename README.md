Test-Task Instruction:
Download Node.js

In Node.js terminal put in:

	cd pathtofile/Test-task
	npm start

The react app should open in your browser labeled Test Task 

Open another node.js terminal and put in:

	cd pathtofile/Test-task
	node.js server.js

The console should say its listening on port 8080.
Now in your browser on Test Task, input what you would like in the email. 

Press review to view what the email sent will be and edit anything you want to change in the input form

Press Direct Send to send the Email 

Components:

The Header component holds the header of the page as well as the logo and the company details

The Input component holds all the form information for the users input, as well as the state of the input

The Email rev holds the formatting for the review of the email after pressing the review button