const express = require('express');
const app = express();
const nodemailer = require('nodemailer');
const port = process.env.PORT || 8080;
//to get past cors policy
const cors = require('cors');
app.use(cors());

app.use(express.json());

require('dotenv').config();

//Nodemailer endpoint, all credentials stored on .env
var transport = {
  host: process.env.emailhost,
  port: process.env.emailport,
  auth: {
    user: process.env.emailuser,
    pass: process.env.emailpass
  }
};

var transporter = nodemailer.createTransport(transport)

transporter.verify((error, success) => {
  if (error) {
    console.log(error);
  } else {
    console.log('All works fine, congratz!');
  }
});

//On post get the data stored in '/' and set up the format of the mail
app.post('/', (req, res, next) => {
  const invnum = req.body.invnum
  const rec = req.body.rec
  const address = req.body.address
  const date = req.body.date
  const subject = req.body.subject
  const invtot = req.body.invtot
  const paydet = req.body.paydet
  console.log(req.body)
  //how the mail will be formated and stylized as well as to which email
  //it is being sent too
  var mail = {
    from: "RAPID DEVELOPER INC.",
    to: 'test-developers@tbecanada.com', 
    subject: subject,
    //formatted to what the email example had shown, img sent as attachment
    html:`
    <html>
      <head style="font-family:Calibri;font-size: 11pt;display:inline-block;">
      <img src="cid:emaillogo" style="width:90px;height:90px;float:left;"/>
      <p>RAPID DEVELOPERS INC.</p>
      <p>1904-777 BAY ST,</p>
      <p>TORONTO ON M5B 2H7</p>
      </head>
      <style>
        table, th, td{
          border: 2px solid black;
          border-collapse:collapse;
          white-space: pre-wrap;
        }
      </style>
      <body style="font-family:Calibri;font-size: 11pt;">
        <p style="font-family:Calibri Light;font-size: 13pt; color:#2E74B5;">INVOICE: #${invnum}</p>
        <p>TO: ${rec}</p>
        <p>${address}</p>
        <p>DATE: ${date}</p>
        <p>${subject}</p>
        <p><b>Invoice total: $${invtot}</b>(see details below)</p>
        <table>
        <tr>
        <th style="width:300px;">Description</th>
        <th style="width:50px;">CAD$</th>
        </tr>
        <tr>
        <td style="white-space:pre-wrap;">${paydet}</td>
        <td>${invtot}</td>
        </tr>
        </table>
      </body>
    </html>`,
    attachments:[{
      filename:'logo.png',
      path:'./src/components/logo.png',
      cid:'emaillogo'
    }]
  }
  //Send the mail that was just formated
  transporter.sendMail(mail, (err) => {
    if (err) {
      res.json({
        msg: 'fail'
      })
    } else {
      res.json({
        msg: 'success'
      })
    }
  })
})

// listen for the port, console log that this is taking place
app.listen(port, () => console.log(`Listening on port ${port}`));