import Logo from './Logo.PNG'

//Format the header in a seperate component
const Header = ({ title }) => {
    return (
        <header>
            <h1>
                {title}
            </h1>
            <img src={Logo} className='logo'/>
            <p className='top'>
            RAPID DEVELOPERS INC.
            </p>
            <p className='top'>
            1904-777 BAY ST, 
            </p>
            <p className='top'>
            TORONTO ON M5B 2H7
            </p>
        </header>
    )
}

export default Header;
