import {useState} from 'react'

//this is to allow states to get set
const Input = ({ make, send }) => {
const[invnum, setInvnum] = useState('')
const[rec, setRec] = useState('')
const[date, setDate] = useState('')
const[address, setAddress] = useState('')
const[subject, setSubject] = useState('')
const[invtot, setInvtot] = useState('')
const[paydet, setPaydet] = useState('')

//when they press review, the states will be sent to app.js where 'email' gets set
//with the values sent
const onSubmit = (e) => {
    e.preventDefault()
    make({invnum, rec, date, address, subject, invtot, paydet})
}
//when direct send is pressed, the sam is in onSubmit will occur,
//but the form gets cleared as the data is already sent and uneditable
const onClick = (e) => {
    e.preventDefault()
    send({invnum, rec, date, address, subject, invtot, paydet})
    setInvnum('')
    setRec('')
    setDate('')
    setAddress('')
    setSubject('')
    setInvtot('')
    setPaydet('')
}

    return (
        //this form will take the users input to fill out the email
        //it will set a new state when ever the input gets changed.
        //When the submit or button gets pressed it calls its respective function
        <form className='email-form' onSubmit={onSubmit}>
            <div className='form-control'>
                <label>INVOICE:</label>
                <input type='text' placeholder='001' 
                value={invnum} onChange={(e) => setInvnum(e.target.value)}/>
            </div>
            <div className='form-control'>
                <label>TO:</label>
                <input type='text' placeholder='Receiver'
                value={rec} onChange={(e) => setRec(e.target.value)}/>
            </div>
            <div className='form-control'>
                <label>Address:</label>
                <input type='text' placeholder='Street, city province postal code'
                value={address} onChange={(e) => setAddress(e.target.value)}/>
            </div>
            <div className='form-control'>
                <label>DATE:</label>
                <input type='text' placeholder='day-month-year'
                value={date} onChange={(e) => setDate(e.target.value)}/>
            </div>
            <div className='form-control'>
                <label>Subject:</label>
                <input type='text' placeholder='Rent payment for ...'
                value={subject} onChange={(e) => setSubject(e.target.value)}/>
            </div>
            <div className='form-control'>
                <label>Invoice Total:</label>
                <input type='text' placeholder='100'
                value={invtot} onChange={(e) => setInvtot(e.target.value)}/>
            </div>
            <div className='form-control'>
                <label>Payment Details:</label>
                <textarea type='text' placeholder='-Item 1'
                value={paydet} onChange={(e) => setPaydet(e.target.value)} rows='5' columns='50'></textarea>     </div>

            <input type='submit' value='Review' className='btn'/>
            <button type='button' value='Send' onClick={onClick} className='btn'>Direct Send</button>
        </form>
    )
}

export default Input
