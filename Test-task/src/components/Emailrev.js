import Logo from './Logo.PNG'

//Set up the format for the review of the email
const Emailrev = ({ email }) => {
    return (
        <div className='review'>
        <img src={Logo} className='logo'/>
        <div className='emailHeader'>
        
        <p className='spacing'>RAPID DEVELOPERS INC.</p>
        <p className='spacing'>1904-777 BAY ST,</p>
        <p className='spacing'>TORONTO ON M5B 2H7</p>
        </div>
        <div>
        <p className='invoice'>INVOICE: #{email.invnum}</p>
        <div className='emailBody'>
        <p>TO: {email.rec}</p>
        <p>{email.address}</p>
        <p>DATE: {email.date}</p>
        <p>{email.subject}</p>
        <p><b>Invoice total: ${email.invtot}</b>(see details below)</p>
        <table className='invtable'>
        <tr>
        <th className='th1'>Description</th>
        <th>CAD$</th>
        </tr>
        <tr>
        <td>{email.paydet}</td>
        <td>${email.invtot}</td>
        </tr>
        </table>
        </div>
        </div>
    </div>
    )
}


export default Emailrev
