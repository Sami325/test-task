import { useState } from 'react'
import Header from './components/Header'
import Input from './components/Input'
import Emailrev from './components/Emailrev'
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import axios from 'axios';

const App = () => {
  //This will hold the submitted input states
  const [email, setEmail] = useState(
    {    
    invnum: '',
    rec:'',
    address:'',
    date:'',
    subject:'',
    invtot:'',
    paydet:'',
    }
    )
  //this takes the email states and sets 'email' values to what the user wanted
  const emailMake = (email) => {
    const madeEmail = {...email}
    setEmail(madeEmail)
  }
  //Same as email make but it uses the http client axios to listen on port 8080(nodemailer)
  //and sends a post request with data that is currently being held in 'email'
  const sendEmail = (email) => {
    const madeEmail = {...email}
    setEmail(madeEmail)
  axios({
    method: "POST", 
    url:"http://localhost:8080/", 
    data: {
    invnum: email.invnum,
    rec: email.rec,
    address: email.address,
    date: email.date,
    subject: email.subject,
    invtot: email.invtot,
    paydet: email.paydet,
    }
})//Checking if the email reaches its destination or not
.then((response)=>{     
  if (response.data.msg === 'success'){
    //react-toastifty notification for Email being sent
    toast("Email sent!");
  }
  else if(response.data.msg === 'fail'){
    //react-toastifty notification for Email not getting sent
    toast("Email not sent, please try again");
  }
})
  }
  return (
    <div className='container'>
      <Header/>
      <Input make={emailMake} send={sendEmail}/>
      <Emailrev email={email}/>
      <ToastContainer/>
    </div>
  )
}



export default App;
